Role Name
=========

This role creates an SSL certificate able to sign other certificates. It is not meant to be
used in real production environments, and more to be used in testing or in small private 
networks.

Requirements
------------

Uses `community.crypto`. See the `requirements.yml` file for more details.

Role Variables
--------------

 - `ca_name`: Name of the certificate authority. This string will be used as Common Name
   and in the directory structure created on the server.

Dependencies
------------

None.

Example Playbook
----------------

This will generate a `default-ca` named certificate authority.

```yml
- hosts: all
  gather_facts: no
  become: yes
  roles:

    - name: certificate_authority
      tags: ["ca"]
```

To specify a custom name just set `ca_name`:

```yml
- hosts: all
  gather_facts: no
  become: yes
  roles:

    - name: certificate_authority
      vars:
        ca_name: myca
      tags: ["ca"]
```

License
-------

[GPL 2.0 Or later](https://spdx.org/licenses/GPL-2.0-or-later.html).
